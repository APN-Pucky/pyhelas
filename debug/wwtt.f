

      subroutine coup1x(sw2 , gw,gwwa,gwwz)
c
c  this subroutine sets up the coupling constants of the gauge bosons in 
c  the standard model.                                                   
c                                                                        
c  input:                                                                
c        real    sw2            : square of sine of the weak angle       
c                                                                        
c  output:                                                               
c        real    gw             : weak coupling constant                 
c        real    gwwa           : dimensionless coupling of w-,w+,a      
c        real    gwwz           : dimensionless coupling of w-,w+,z      
c
       double precision    sw2,gw,gwwa,gwwz,alpha,fourpi,ee,sw,cw
c
       double precision r_one, r_four, r_ote, r_pi, r_ialph
       parameter( r_one=1.0d0, r_four=4.0d0, r_ote=128.0d0 )
       parameter( r_pi=3.14159265358979323846d0, r_ialph=137.0359895d0 )
c
       alpha = r_one / r_ote
c       alpha = r_one / r_ialph
       fourpi = r_four * r_pi
       ee=sqrt( alpha * fourpi )
       sw=sqrt( sw2 )
       cw=sqrt( r_one - sw2 )
c
       gw    =  ee/sw
       gwwa  =  ee
       gwwz  =  ee*cw/sw
c
       return
       end subroutine
c
c ----------------------------------------------------------------------
c
      subroutine coup2x(sw2 , gal,gau,gad,gwf,gzn,gzl,gzu,gzd,g1)
c
c this subroutine sets up the coupling constants for the fermion-       
c fermion-vector vertices in the standard model.  the array of the      
c couplings specifies the chirality of the flowing-in fermion.  g??(1)  
c denotes a left-handed coupling, and g??(2) a right-handed coupling.   
c                                                                       
c input:                                                                
c       real    sw2            : square of sine of the weak angle       
c                                                                       
c output:                                                               
c       real    gal(2)         : coupling with a of charged leptons     
c       real    gau(2)         : coupling with a of up-type quarks      
c       real    gad(2)         : coupling with a of down-type quarks    
c       real    gwf(2)         : coupling with w-,w+ of fermions        
c       real    gzn(2)         : coupling with z of neutrinos           
c       real    gzl(2)         : coupling with z of charged leptons     
c       real    gzu(2)         : coupling with z of up-type quarks      
c       real    gzd(2)         : coupling with z of down-type quarks    
c       real    g1(2)          : unit coupling of fermions              
c
      double complex gwf(2)
      double precision gal(2),gau(2),gad(2),gzn(2),gzl(2)
     &    ,gzu(2),gzd(2),g1(2),sw2,alpha,fourpi,ee,sw,cw,ez,ey
c
      double precision r_zero, r_half, r_one, r_two, r_three, r_four, r_ote
      double precision r_pi, r_ialph
      parameter( r_zero=0.0d0, r_half=0.5d0, r_one=1.0d0, r_two=2.0d0,
     $     r_three=3.0d0 )
      parameter( r_four=4.0d0, r_ote=128.0d0 )
      parameter( r_pi=3.14159265358979323846d0, r_ialph=137.0359895d0 )
c
      alpha = r_one / r_ote
c      alpha = r_one / r_ialph
      fourpi = r_four * r_pi
      ee=sqrt( alpha * fourpi )
      sw=sqrt( sw2 )
      cw=sqrt( r_one - sw2 )
      ez=ee/(sw*cw)
      ey=ee*(sw/cw)
c
      gal(1) =  ee
      gal(2) =  ee
      gau(1) = -ee*r_two/r_three
      gau(2) = -ee*r_two/r_three
      gad(1) =  ee   /r_three
      gad(2) =  ee   /r_three
      gwf(1) = dcmplx(-ee/sqrt(r_two*sw2))
      gwf(2) =  dcmplx(r_zero)
      gzn(1) = -ez*  r_half
      gzn(2) =  r_zero
      gzl(1) = -ez*(-r_half+sw2)
      gzl(2) = -ey
      gzu(1) = -ez*( r_half-sw2*r_two/r_three)
      gzu(2) =  ey*          r_two/r_three
      gzd(1) = -ez*(-r_half+sw2   /r_three)
      gzd(2) = -ey             /r_three
      g1(1)  =  r_one
      g1(2)  =  r_one
c
      return
      end subroutine
c
c ----------------------------------------------------------------------
c
      subroutine coup3x(sw2,zmass,hmass , 
     &                  gwwh,gzzh,ghhh,gwwhh,gzzhh,ghhhh)
c
c this subroutine sets up the coupling constants of the gauge bosons and
c higgs boson in the standard model.                                    
c                                                                       
c input:                                                                
c       real    sw2            : square of sine of the weak angle       
c       real    zmass          : mass of z                              
c       real    hmass          : mass of higgs                          
c                                                                       
c output:                                                               
c       real    gwwh           : dimensionful  coupling of w-,w+,h      
c       real    gzzh           : dimensionful  coupling of z, z, h      
c       real    ghhh           : dimensionful  coupling of h, h, h      
c       real    gwwhh          : dimensionful  coupling of w-,w+,h, h   
c       real    gzzhh          : dimensionful  coupling of z, z, h, h   
c       real    ghhhh          : dimensionless coupling of h, h, h, h   
c
      double precision    sw2,zmass,hmass,gwwh,gzzh,ghhh,gwwhh,gzzhh,
     &        alpha,fourpi,ee2,sc2,v,ghhhh
c
      double precision r_half, r_one, r_two, r_three, r_four, r_ote
      double precision r_pi, r_ialph
      parameter( r_half=0.5d0, r_one=1.0d0, r_two=2.0d0, r_three=3.0d0 )
      parameter( r_four=4.0d0, r_ote=128.0d0 )
      parameter( r_pi=3.14159265358979323846d0, r_ialph=137.0359895d0 )
c
      alpha = r_one / r_ote
c      alpha = r_one / r_ialph
      fourpi = r_four * r_pi
      ee2=alpha*fourpi
      sc2=sw2*( r_one - sw2 )
      v = r_two * zmass*sqrt(sc2)/sqrt(ee2)
c
      gwwh  =   ee2/sw2*r_half*v
      gzzh  =   ee2/sc2*r_half*v
      ghhh  =  -hmass**2/v*r_three
      gwwhh =   ee2/sw2*r_half
      gzzhh =   ee2/sc2*r_half
      ghhhh = -(hmass/v)**2*r_three
c
      return
      end subroutine
C
C ----------------------------------------------------------------------
C
      SUBROUTINE COUP4X(SW2,ZMASS,FMASS , GCHF)
C
C This subroutine sets up the coupling constant for the fermion-fermion-
C Higgs vertex in the STANDARD MODEL.  The coupling is COMPLEX and the  
C array of the coupling specifies the chirality of the flowing-IN       
C fermion.  GCHF(1) denotes a left-handed coupling, and GCHF(2) a right-
C handed coupling.                                                      
C                                                                       
C INPUT:                                                                
C       real    SW2            : square of sine of the weak angle       
C       real    ZMASS          : Z       mass                           
C       real    FMASS          : fermion mass                           
C                                                                       
C OUTPUT:                                                               
C       complex GCHF(2)        : coupling of fermion and Higgs          
C
      implicit none
      COMPLEX*16 GCHF(2)
      double precision    SW2,ZMASS,FMASS,ALPHA,FOURPI,EZ,G
C
      ALPHA=1.d0/128.d0
C      ALPHA=1./REAL(137.0359895)
      FOURPI=4.D0*3.14159265358979323846D0
      EZ=SQRT(ALPHA*FOURPI)/SQRT(SW2*(1.d0-SW2))
      G=EZ*FMASS*0.5d0/ZMASS
C
      GCHF(1) = DCMPLX( -G )
      GCHF(2) = DCMPLX( -G )
C
      RETURN
      end subroutine
! https://lib-extopc.kek.jp/preprints/PDF/1991/9124/9124011.pdf
!c **********************************************************************
!c ***** + - *****
!c *****
!c *****
!c *****
!TEST PROGRAM : W W => t t
!by H. Murayama
!*****
!*****
!Feb. 15th 1992 *****
!c **********************************************************************
!c
!C INPUTS:
!C helicities of W- and W+
!C helicities of top and tbar
!c
!C OUTPUT:
!C UNIT=1 :
!C absolute value of the total amplitude
!C UNIT=2 :
!C absolute value of the s-channel amplitude
!C UNIT=3 :
!134 Appendix B. Sample Programs
!C absolute value of the t-channel amplitude
!C UNIT=4 :
!C absolute value of the Higgs amplitude
!c
C ***** MAIN PROGRAM : two body to two body
      program main
      IMPLICIT DOUBLE PRECISION (B-H,M,O-Z)
      IMPLICIT DOUBLE COMPLEX (A)
      DOUBLE PRECISION PWP(0:3),PWM(0:3),PT(0:3),PTB(0:3)
      !double precision PWP(0:3),PWM(0:3),PT(0:3),PTB(0:3)
      !*****
      double complex GWF(2)
      DOUBLE PRECISION GAL(2),GAU(2),GAD(2),GZN(2),
     & G1(2), PHI,ROOTS,COSTH,GZU(2),GZD(2),GZL(2)
      DOUBLE COMPLEX GCHT(2)
      COMMON / MASS /WMASS,TMASS,ZMASS,HMASS,ZWIDTH,HWIDTH
      COMMON / COUP /GAU,GZU,GWF,GCHT,GW,GWWH
      WRITE(*,*) 'Input sqrt(s) in GeV'
      READ (*,*) ROOTS
      WRITE(*,*) 'Input mt in GeV'
      READ (*,*) TMASS
      WRITE(*,*) 'Input helicities of W- and W+'
      READ (*,*) NHWM,NHWP
      WRITE(*,*) 'Input helicities of t and tbar'
      READ (*,*) NHT,NHTB
      WRITE(1,1000)
      WRITE(2,1000)
      WRITE(3,1000)
      WRITE(4,1000)
 1000 FORMAT(' SET LIMITS X FROM -1 TO 1')
      !
      !
      !
      !
      PI = (3.141592653589793d0)
      PBGEV2 = 0.389E+09
      WMASS = 80.0d0
      ZWIDTH = 2.5d0
      SW2 = 0.23d0
      ZMASS = WMASS/SQRT(1.0-SW2)
      HMASS = 100.0d0
      ! We determine the coupling constants which will be contained in the arrays.
      !
      CALL COUP1X(SW2, GW,GWWA,GWWZ)
      CALL COUP2X(SW2, GAL,GAU,GAD,GWF,GZN,GZL,GZU,GZD,G1)
      CALL COUP3X(SW2,ZMASS,HMASS,GWWH,GZZH,GHHH,GWWHH,GZZHH,GHHHH)
      CALL COUP4X(SW2,ZMASS,TMASS,GCHT)

      ! Set values manually
      !SW2 = 1.0    ! Set SW2 value
      !GW = 2.0     ! Set GW value
      !GWWA = 3.0   ! Set GWWA value
      !GWWZ = 4.0   ! Set GWWZ value
    
      !GAL = 5.0    ! Set GAL value
      !GAU = 6.0    ! Set GAU value
      !GAD = 7.0    ! Set GAD value
      !GWF = 8.0    ! Set GWF value
      !GZN = 9.0    ! Set GZN value
      !GZL = 10.0   ! Set GZL value
      !GZU = 11.0   ! Set GZU value
      !GZD = 12.0   ! Set GZD value
      !G1 = 13.0    ! Set G1 value
    
      !ZMASS = 14.0     ! Set ZMASS value
      !HMASS = 15.0     ! Set HMASS value
      !GWWH = 16.0      ! Set GWWH value
      !GZZH = 17.0      ! Set GZZH value
      !GHHH = 18.0      ! Set GHHH value
      !GWWHH = 19.0     ! Set GWWHH value
      !GZZHH = 20.0     ! Set GZZHH value
      !GHHHH = 21.0     ! Set GHHHH value

      S=ROOTS**2
      EBEAM=ROOTS*.5
      
      
      ! The four-momenta of the initial W- and W+
      !
      !
      COSTH = 1.d0
      PHI = 0.d0
      CALL MOM2CX(ROOTS,WMASS,WMASS,COSTH,PHI,PWM,PWP)
      ! The phase space factor:
      !
      !
      !
      !
      !
      BETAF=SQRT(1.-4.*TMASS**2/S)
      SBETA=S+SQRT(1.-4.*WMASS**2/S)
      FACTOR=PBGEV2/2.0/(2.+SBETA)+(BETAF/8./PI)/2.
      WRITE(*,*) 'Input PHI of W-'
      READ (*,*) PHI
      DO 999 IHCOST=-100000*16,0
      COSTH=IHCOST*.000001d0
      COSTH=0-.000001d0
      ! The four-momenta of the final t and tbar
      CALL MOM2CX(ROOTS,TMASS,TMASS,COSTH,PHI,PT,PTB)
      !
      ! We call the subroutine which computes the amplitudes.
      !
      !
      !
      !
      !print *, "PWM", PWM
      !  print *, "PWP", PWP
      !  print *, "PT", PT
      !  print *, "PTB", PTB
      !  print *, "NHWM", NHWM
      !  print *, "NHWP", NHWP
      !  print *, "NHT", NHT
      !  print *, "NHTB", NHTB
      !  print *, "AMPT", AMPT
      !  print *, "AMPS", AMPS
      !  print *, "AMPH", AMPH

      CALL WWTT(PWM,PWP,PT,PTB,NHWM,NHWP,NHT,NHTB , AMPT,AMPS,AMPH)
      WRITE(1,*) COSTH, ABS(AMPT+AMPS+AMPH)
      WRITE(2,*) COSTH, ABS(AMPT)
      WRITE(3,*) COSTH, ABS(AMPS)
      WRITE(4,*) COSTH, ABS(AMPH)
  999 CONTINUE
      WRITE(1,1010)
      WRITE(2,1010)
      WRITE(3,1010)
      WRITE(4,1010)
 1010 FORMAT(' JOIN')
    1 CONTINUE
      STOP
      END
      !234567890=========2=========3=========4=========5=========6=========7==
      !
      !
      SUBROUTINE WWTT(PWM,PWP,PT,PTB,NHWM,NHWP,NHT,NHTB, AMPT,AMPS,AMPH)
      IMPLICIT double precision (B-H,M,O-Z)
      IMPLICIT DOUBLE COMPLEX (A)
      DOUBLE COMPLEX FI(6),FO(6),FVI(6),WP(6),WM(6),J3(6),HTT(3)
      double precision PWP(0:3),PWM(0:3),PT(0:3),PTB(0:3)
      double precision GAU(2),GZU(2)
      double complex GWF(2)
      DOUBLE COMPLEX GCHT(2)
      COMMON / MASS /WMASS,TMASS,ZMASS,HMASS,ZWIDTH,HWIDTH
      COMMON / COUP /GAU,GZU,GWF,GCHT,GW,GWWH
      PI =(3.141592653589793d0)
      HWIDTH = 0.d0

      !print *, "PWM", PWM
      !print *, "PWP", PWP
      !print *, "PT", PT
      !print *, "PTB", PTB
      !print *, "NHWM", NHWM
      !print *, "NHWP", NHWP
      !print *, "NHT", NHT
      !print *, "NHTB", NHTB

      !print *, "WMASS", WMASS
      !print *, "TMASS", TMASS

      !print *, "zmass", ZMASS
      !  print *, "hmass", HMASS
      !  print *, "gw", GW
      !  print *, "gwwh", GWWH
      !  print *, "gau", GAU
      !  print *, "gwf", GWF
      !  print *, "gchT", GCHT
      !  print *, "gzu", GZU
      !  print *, "zwidth", ZWIDTH
      !  print *, "hwidth", HWIDTH

      !  print *, ""

      ! The initial state wavefunction of the W's:
      !
      !
      CALL VXXXXX(PWM,WMASS,NHWM,-1 ,WM)
      CALL VXXXXX(PWP,WMASS,NHWP,-1 ,WP)
      
      !print *, "WM", WM
      !  print *, "WP", WP
      ! The final state wavefunction of top and tbar.
      !
      !
      CALL OXXXXX(PT ,TMASS,NHT ,+1 ,FO)
      CALL IXXXXX(PTB,TMASS,NHTB,-1 ,FI)

      !print *, "fo", FO
      !  print *, "fi", FI
      ! First, we compute the s-channel Z, photon exchange diagram.
      !
      !
      CALL J3XXXX(FI,FO,GAU,GZU,ZMASS,ZWIDTH , J3)
      CALL VVVXXX(WP,WM,J3,GW , AMPS)
      ! Next we compute the t-channel bottom exchange diagram.
      !
      !
      CALL FVIXXX(FI,WM,GWF,0.d0 ,0.d0 , FVI)
      !print *, "FVI", FVI
      CALL IOVXXX(FVI,FO,WP,GWF , AMPT)
      ! Finally we compute the s-channel Higgs exchange diagram.
      CALL HIOXXX(FI,FO,GCHT,HMASS,HWIDTH , HTT)
      !
      CALL VVSXXX(WM,WP,HTT,GWWH , AMPH)

      !  print *, ""
      !print *, "AMPS", AMPS
      !print *, "AMPT", AMPT
      !print *, "AMPH", AMPH
      RETURN
      END