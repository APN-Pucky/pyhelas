      subroutine oxxxxx(p,fmass,nhel,nsf , fo)
c
c This subroutine computes a fermion wavefunction with the flowing-OUT
c fermion number.
c
c input:
c       real    p(0:3)         : four-momentum of fermion
c       real    fmass          : mass          of fermion
c       integer nhel = -1 or 1 : helicity      of fermion
c       integer nsf  = -1 or 1 : +1 for particle, -1 for anti-particle
c
c output:
c       complex fo(6)          : fermion wavefunction               <fo|
c     
      implicit none
      double complex fo(6),chi(2)
      double precision p(0:3),sf(2),sfomeg(2),omega(2),fmass,
     &     pp,pp3,sqp0p3,sqm(0:1)
      integer nhel,nsf,nh,ip,im

      double precision rZero, rHalf, rTwo
      parameter( rZero = 0.0d0, rHalf = 0.5d0, rTwo = 2.0d0 )

#ifdef HELAS_CHECK
      double precision p2
      double precision epsi
      parameter( epsi = 2.0d-5 )
      integer stdo
      parameter( stdo = 6 )
#endif
c
#ifdef HELAS_CHECK
      pp = sqrt(p(1)**2+p(2)**2+p(3)**2)
      if ( abs(p(0))+pp.eq.rZero ) then
         write(stdo,*)
     &        ' helas-error : p(0:3) in oxxxxx is zero momentum'
      endif
      if ( p(0).le.rZero ) then
         write(stdo,*)
     &        ' helas-error : p(0:3) in oxxxxx has non-positive energy'
         write(stdo,*)
     &        '         : p(0) = ',p(0)
      endif
      p2 = (p(0)-pp)*(p(0)+pp)
      if ( abs(p2-fmass**2).gt.p(0)**2*epsi ) then
         write(stdo,*)
     &        ' helas-error : p(0:3) in oxxxxx has inappropriate mass'
         write(stdo,*)
     &        '             : p**2 = ',p2,' : fmass**2 = ',fmass**2
      endif
      if ( abs(nhel).ne.1 ) then
         write(stdo,*) ' helas-error : nhel in oxxxxx is not -1,1'
         write(stdo,*) '             : nhel = ',nhel
      endif
      if ( abs(nsf).ne.1 ) then
         write(stdo,*) ' helas-error : nsf in oxxxxx is not -1,1'
         write(stdo,*) '             : nsf = ',nsf
      endif
#endif

      fo(5) = dcmplx(p(0),p(3))*nsf
      fo(6) = dcmplx(p(1),p(2))*nsf

      nh = nhel*nsf

      if ( fmass.ne.rZero ) then

         pp = min(p(0),dsqrt(p(1)**2+p(2)**2+p(3)**2))

         if ( pp.eq.rZero ) then
            
            sqm(0) = dsqrt(abs(fmass)) ! possibility of negative fermion masses
            sqm(1) = sign(sqm(0),fmass) ! possibility of negative fermion masses
            ip = -((1+nh)/2)
            im =  (1-nh)/2
            
            fo(1) = im     * sqm(im)
            fo(2) = ip*nsf * sqm(im)
            fo(3) = im*nsf * sqm(-ip)
            fo(4) = ip     * sqm(-ip)
            
         else
            
            pp = min(p(0),dsqrt(p(1)**2+p(2)**2+p(3)**2))
            sf(1) = dble(1+nsf+(1-nsf)*nh)*rHalf
            sf(2) = dble(1+nsf-(1-nsf)*nh)*rHalf
            omega(1) = dsqrt(p(0)+pp)
            omega(2) = fmass/omega(1)
            ip = (3+nh)/2
            im = (3-nh)/2
            sfomeg(1) = sf(1)*omega(ip)
            sfomeg(2) = sf(2)*omega(im)
            pp3 = max(pp+p(3),rZero)
            chi(1) = dcmplx( dsqrt(pp3*rHalf/pp) )
            if ( pp3.eq.rZero ) then
               chi(2) = dcmplx(-nh )
            else
               chi(2) = dcmplx( nh*p(1) , -p(2) )/dsqrt(rTwo*pp*pp3)
            endif
            
            fo(1) = sfomeg(2)*chi(im)
            fo(2) = sfomeg(2)*chi(ip)
            fo(3) = sfomeg(1)*chi(im)
            fo(4) = sfomeg(1)*chi(ip)

            print *, 'sfomeg', sfomeg
            print *, 'omega', omega
            print *, 'chi', chi

         endif
         
      else
         
         if(p(1).eq.0d0.and.p(2).eq.0d0.and.p(3).lt.0d0) then
            sqp0p3 = 0d0
         else
            sqp0p3 = dsqrt(max(p(0)+p(3),rZero))*nsf
         end if
         chi(1) = dcmplx( sqp0p3 )
         if ( sqp0p3.eq.rZero ) then
            chi(2) = dcmplx(-nhel )*dsqrt(rTwo*p(0))
         else
            chi(2) = dcmplx( nh*p(1), -p(2) )/sqp0p3
         endif
         if ( nh.eq.1 ) then
            fo(1) = chi(1)
            fo(2) = chi(2)
            fo(3) = dcmplx( rZero )
            fo(4) = dcmplx( rZero )
         else
            fo(1) = dcmplx( rZero )
            fo(2) = dcmplx( rZero )
            fo(3) = chi(2)
            fo(4) = chi(1)
         endif
         
      endif
c
      return
      end


c main

        program main
        implicit none
        double complex fic(6),foc(6),sc(3),gc(2)
        double complex vertex,j3(6),cg
        double precision pwm(4), wmass,width
        double precision zmass,zwidth,g
        double precision rfic(6),rfoc(6),rsc(3),rgc(2)
        double precision cfic(6),cfoc(6),csc(3),cgc(2)
        double precision rj3(6),cj3(6)
        external ioscxx,j3xxxx
        integer i
        integer nhwm,is
        integer seed(8)
        seed = (/1337, 1337,1337,1337,1337, 1337,1337,1337/)

        zmass = 91.187d0
        zwidth = 2.5d0
        wmass = 80.4d0
        width = 2.1d0
        nhwm = 1
        is = -1
        g = 1.0d0
        cg = dcmplx(1.0d0,0.0d0)

        call random_seed(put=seed)

        !do i=1,10000000

        call random_number(rfic)
        call random_number(rfoc)
        call random_number(rsc)
        call random_number(rgc)
        call random_number(rj3)
        call random_number(pwm)


        call random_number(cfic)
        call random_number(cfoc)
        call random_number(csc)
        call random_number(cgc)
        call random_number(cj3)

        fic = dcmplx(rfic,cfic)
        foc = dcmplx(rfoc,cfoc)
        foc = dcmplx(rj3,cj3)
        sc = dcmplx(rsc,csc)
        gc = dcmplx(rgc,cgc)

        call ioscxx(fic,foc,sc,gc,vertex)
        call j3xxxx(fic,foc,sc,gc,zmass,zwidth,j3)
        call vvvxxx(fic,foc,j3,g,vertex)
        !enddo 

        print *,'fic', fic
        print *,'foc', foc
        print *,'sc', sc
        print *,'gc', gc
        print *,'g', g
        print *,'zmass' , zmass
        print *,'zwidth', zwidth
        print *,'ioscxx', vertex
        print *,'j3', j3
        print *,'vertex', vertex

        call vxxxxx(pwm,wmass,nhwm,is,j3)
        print *,'pwm', pwm
        print *,'wmass', wmass
        print *,'nhwm', nhwm
        print *,'is', is
        print *,'vxxxxx', j3

        call vxxxxx(pwm,0d0,-nhwm,-is,j3)
        print *,'pwm', pwm
        print *,'wmass', 0
        print *,'nhwm', -nhwm
        print *,'is', -is
        print *,'vxxxxx', j3

        call oxxxxx(pwm,wmass,nhwm,is,j3)
        print *,'pwm', pwm
        print *,'wmass', wmass
        print *,'nhwm', nhwm
        print *,'is', is
        print *,'oxxxxx', j3

        call oxxxxx(pwm,0d0,-nhwm,-is,j3)
        print *,'pwm', pwm
        print *,'wmass', 0
        print *,'nhwm', -nhwm
        print *,'is', -is
        print *,'oxxxxx', j3


        call ixxxxx(pwm,wmass,nhwm,is,j3)
        print *,'pwm', pwm
        print *,'wmass', wmass
        print *,'nhwm', nhwm
        print *,'is', is
        print *,'ixxxxx', j3

        call ixxxxx(pwm,0d0,-nhwm,-is,j3)
        print *,'pwm', pwm
        print *,'wmass', 0
        print *,'nhwm', -nhwm
        print *,'is', -is
        print *,'ixxxxx', j3

        call fvixxx(fic,foc,gc,wmass,width,j3)
        print *,'fic', fic
        print *,'foc', foc
        print *,'gc', gc
        print *,'wmass', wmass
        print *,'width', width
        print *,'fvixxx', j3

        call iovxxx(fic,foc,j3,gc,vertex)
        print *,'fic', fic
        print *,'foc', foc
        print *,'j3', j3
        print *,'gc', gc
        print *,'iovxxx', vertex

        call hioxxx(fic,foc,gc,wmass,width,sc)
        print *,'fic', fic
        print *,'foc', foc
        print *,'gc', gc
        print *,'wmass', wmass
        print *,'width', width
        print *,'hioxxx', sc

        call vvsxxx(fic,foc,sc,cg,vertex)
        print *,'fic', fic
        print *,'foc', foc
        print *,'sc', sc
        print *,'g', cg
        print *,'vvsxxx', vertex

        end
