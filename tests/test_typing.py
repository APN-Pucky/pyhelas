import jax.numpy as jnp

import pyhelas
from pyhelas.ixxxxx import ixxxxx_scalar
from pyhelas.oxxxxx import oxxxxx_scalar
from pyhelas.typing import Momentum, Vector, OFermion, IFermion, Momentum

def test_typing():
    p1 = Momentum(jnp.array([6500.0, 0.0, 0.0, 6499.5076736626752]))
    p2 = Momentum(jnp.array([6500.0, -0.0, -0.0, -6499.5076736626752]))
    p3 = Momentum(jnp.array([6500.0, 3511.3664484306901, 5468.6292308800057, 0.0]))
    p4 = Momentum(jnp.array([6500.0, -3511.3664484306901, -5468.6292308800057, -0.0]))

    # incoming fermion
    f1 = ixxxxx_scalar(p1, 0.0, 1, 1)
    f2 = oxxxxx_scalar(p2, 0.0, 1, -1)