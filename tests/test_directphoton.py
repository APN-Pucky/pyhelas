import pyhelas
import jax.numpy as jnp

def test_directphoton():
    #"qq_ -> ag"
    p1 = jnp.array([[6500.0, 0.0, 0.0, 6499.5076736626752]])
    p2 = jnp.array([[6500.0, -0.0, -0.0, -6499.5076736626752]])
    p3 = jnp.array([[6500.0, 3511.3664484306901, 5468.6292308800057, 0.0]])
    p4 = jnp.array([[6500.0, -3511.3664484306901, -5468.6292308800057, -0.0]])

    # incoming fermion
    f1 = pyhelas.ixxxxx(p1, 0.0, 1, 1)
    f2 = pyhelas.oxxxxx(p2, 0.0, 1, -1)

    # outgoing gluon and photon
    g2 = pyhelas.vxxxxx(p3, 0.0, 1, 1)
    a3 = pyhelas.vxxxxx(p4, 0.0, 1, 1)

    fvi = pyhelas.fvixxx(f1, a3, 0.0, 0.0)
    ampu = pyhelas.iovxxx(fvi,f2,g2,0.0)

    #assert jnp.allclose(ampu, [0.0+0.0j]) # TODO fix

    #"qg -> aq"

    f1 = pyhelas.ixxxxx(p1, 0.0,1,1)
    g2 = pyhelas.vxxxxx(p2, 0.0,1,-1)

    f3 = pyhelas.oxxxxx(p3, 0.0,1,1)
    a4 = pyhelas.vxxxxx(p4, 0.0,1,1)

    fvi = pyhelas.fvixxx(f1,g2, 0.0, 0.0)
    amps = pyhelas.iovxxx(fvi,f3,a4,0.0)

    #assert jnp.allclose(amps, [0.0+0.0j]) # TODO fix


