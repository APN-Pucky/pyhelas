import pyhelas
import time
import jax
from jax import jit
import jax.numpy as jnp
import pytest
import tqdm

def test_helicities():
    assert (pyhelas.construct_helicities(1)[0] == jnp.array([1,-1])).all()
    assert (pyhelas.construct_helicities(2)[0] == jnp.array([1,1,-1,-1])).all()
    assert (pyhelas.construct_helicities(2)[1] == jnp.array([1,-1,1,-1])).all()

def test_mom():
    assert (pyhelas.all_helicities([jnp.array([1,2,3,4])])[0][0] == jnp.array([[1,2,3,4],[1,2,3,4]])).all() 
    assert (pyhelas.all_helicities([jnp.array([1,2,3,4])])[1][0] == jnp.array([1,-1])).all() 

    assert (pyhelas.all_helicities([jnp.array([1,2,3,4]),jnp.array([5,6,7,8])])[0][0] == jnp.array([[1,2,3,4],[1,2,3,4],[1,2,3,4],[1,2,3,4]])).all() 
    assert (pyhelas.all_helicities([jnp.array([1,2,3,4]),jnp.array([5,6,7,8])])[0][1] == jnp.array([[5,6,7,8],[5,6,7,8],[5,6,7,8],[5,6,7,8]])).all() 
    assert (pyhelas.all_helicities([jnp.array([1,2,3,4]),jnp.array([5,6,7,8])])[1][0] == jnp.array([1,1,-1,-1])).all() 
    assert (pyhelas.all_helicities([jnp.array([1,2,3,4]),jnp.array([5,6,7,8])])[1][1] == jnp.array([1,-1,1,-1])).all() 

def wwtt(pwm,pwp,pt,ptb,nhwm,nhwp,nht,nhtb):
    wmass = 80.0
    tmass = 120.0
    zwidth = 2.5
    hwidth = 0.0

    zmass = 91.168461167710362
    hmass = 100.0
    gau = jnp.array([-0.20888568955258335+0j, -0.20888568955258335+0j])
    gwf = jnp.array([-0.46197772020596173+0j, 0.0+0j])
    gcht = jnp.array([-0.49000136807210559+0j,-0.49000136807210559+0j])
    gzu = jnp.array([-0.25810876402830546+0j, 0.11416349178175049+0j])
    gw = jnp.array(  0.65333515742947412+0j)
    gwwh = jnp.array(52.266812594357944+0j)
    wm = pyhelas.vxxxxx(pwm,wmass,nhwm, -1)
    wp = pyhelas.vxxxxx(pwp,wmass,nhwp, -1)

    fo = pyhelas.oxxxxx(pt,tmass,nht, 1)
    fi = pyhelas.ixxxxx(ptb,tmass,nhtb, -1)

    j3 = pyhelas.j3xxxx(fi,fo, gau,gzu,zmass,zwidth)
    amps = pyhelas.vvvxxx(wp,wm,j3,gw)

    fvi = pyhelas.fvixxx(fi,wm,gwf,0.,0.)
    ampt = pyhelas.iovxxx(fvi,fo,wp,gwf)

    htt = pyhelas.hioxxx(fi,fo,gcht,hmass,hwidth)
    amph = pyhelas.vvsxxx(wm,wp,htt,gwwh)

    return jnp.array([amps,ampt,amph])

@pytest.mark.parametrize('device', [
    'cpu', 
    'gpu'])
def test_hel_mom_wwtt(device):
    jax.config.update('jax_platform_name', device)

    
    pwm = jnp.array([6500.0, 0.0, 0.0, 6499.5076736626752])
    pwp = jnp.array([6500.0, -0.0, -0.0, -6499.5076736626752])
    pt  = jnp.array([6500.0, 3511.3664484306901, 5468.6292308800057, 0.0])
    ptb = jnp.array([6500.0, -3511.3664484306901, -5468.6292308800057, -0.0])

    ps,hels = pyhelas.all_helicities([pwm,pwp,pt,ptb])

    #tps,thels = pyhelas.all_helicities([pwm,pwp,pt,ptb])
    for i in range(4):
        ps[i] = jnp.repeat(ps[i], 100_000,axis=0)
    for i in range(4):
        hels[i] = jnp.repeat(hels[i], 100_000,axis=0)

    print("Loaded")
    jit_wwtt = jit(wwtt)
    print("Compiled")
    #    for i,p in enumerate(ps):
    #        tps[i] = jnp.concatenate((tps[i],p),axis=0)
    #    for i,h in enumerate(hels):
    #        thels[i] = jnp.concatenate((thels[i],h),axis=0)

    pwm = ps[0]
    pwp = ps[1]
    pt  = ps[2]
    ptb = ps[3]

    nhwm = hels[0]
    nhwp = hels[1]
    nht = hels[2]
    nhtb = hels[3]



    # start time
    start = time.time()

    jit_wwtt(pwm,pwp,pt,ptb,nhwm,nhwp,nht,nhtb)


    # end time
    end = time.time()
    print("Time taken: ", end-start)


if __name__ == "__main__":
    test_hel_mom_wwtt('gpu')
    print("All tests passed gpu!")