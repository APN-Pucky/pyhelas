"""
Original Fortran code:

      subroutine fvixxx(fi,vc,gc,fmass,fwidth , fvi)
c
c This subroutine computes an off-shell fermion wavefunction from a
c flowing-IN external fermion and a vector boson.
c
c input:
c       complex fi(6)          : flow-in  fermion                   |fi>
c       complex vc(6)          : input    vector                      v
c       complex gc(2)          : coupling constants                  gvf
c       real    fmass          : mass  of output fermion f'
c       real    fwidth         : width of output fermion f'
c
c output:
c       complex fvi(6)         : off-shell fermion             |f',v,fi>
c
      implicit none
      double complex fi(6),vc(6),gc(2),fvi(6),sl1,sl2,sr1,sr2,d
      double precision pf(0:3),fmass,fwidth,pf2
      
      double precision rZero, rOne
      parameter( rZero = 0.0d0, rOne = 1.0d0 )
      double complex cImag, cZero
      parameter( cImag = ( 0.0d0, 1.0d0 ), cZero = ( 0.0d0, 0.0d0 ) )

      fvi(5) = fi(5)-vc(5)
      fvi(6) = fi(6)-vc(6)

      pf(0) = dble( fvi(5))
      pf(1) = dble( fvi(6))
      pf(2) = dimag(fvi(6))
      pf(3) = dimag(fvi(5))
      pf2 = pf(0)**2-(pf(1)**2+pf(2)**2+pf(3)**2)

            d = -rOne/dcmplx( pf2-fmass**2, fmass*fwidth )
      sl1 =   (vc(1)+      vc(4))*fi(1)
     &      + (vc(2)-cImag*vc(3))*fi(2)
      sl2 =   (vc(2)+cImag*vc(3))*fi(1)
     &      + (vc(1)-      vc(4))*fi(2)

      if ( gc(2).ne.cZero ) then
         sr1 =   (vc(1)-      vc(4))*fi(3)
     &         - (vc(2)-cImag*vc(3))*fi(4)
         sr2 = - (vc(2)+cImag*vc(3))*fi(3)
     &         + (vc(1)+      vc(4))*fi(4)

         fvi(1) = ( gc(1)*((pf(0)-pf(3))*sl1 - dconjg(fvi(6))*sl2)
     &             +gc(2)*fmass*sr1 )*d
         fvi(2) = ( gc(1)*(      -fvi(6)*sl1 +  (pf(0)+pf(3))*sl2)
     &             +gc(2)*fmass*sr2 )*d
         fvi(3) = ( gc(2)*((pf(0)+pf(3))*sr1 + dconjg(fvi(6))*sr2)
     &             +gc(1)*fmass*sl1 )*d
         fvi(4) = ( gc(2)*(       fvi(6)*sr1 +  (pf(0)-pf(3))*sr2)
     &             +gc(1)*fmass*sl2 )*d

      else
         d = d * gc(1)
         fvi(1) = ((pf(0)-pf(3))*sl1 - dconjg(fvi(6))*sl2)*d
         fvi(2) = (      -fvi(6)*sl1 +  (pf(0)+pf(3))*sl2)*d
         fvi(3) = fmass*sl1*d
         fvi(4) = fmass*sl2*d
      end if
c
      return
      end
"""


import jax.numpy as jnp
from jax import vmap

from pyhelas.typing import IFermion, Vector

def fvixxx(fi : IFermion, vc :Vector, gc, fmass, fwidth) -> IFermion:
    # Define constants
    rZero = 0.0 #jnp.float64(0.0)
    rOne = 1.0 # jnp.float64(1.0)
    cImag = 1j
    cZero = 0.0 + 0.0j

    # Initialize fvi
    fvi = jnp.zeros(6, dtype=complex)

    # Perform calculations
    fvi = fi - vc

    pf = jnp.array([fvi[4].real, fvi[5].real, fvi[5].imag, fvi[4].imag])
    pf2 = pf[0]**2 - (pf[1]**2 + pf[2]**2 + pf[3]**2)

    d = -rOne / (pf2 - fmass**2 + 1j*fmass * fwidth)

    sl1 = (vc[0] + vc[3]) * fi[0] + (vc[1] - cImag * vc[2]) * fi[1]
    sl2 = (vc[1] + cImag * vc[2]) * fi[0] + (vc[0] - vc[3]) * fi[1]

    sr1 = (vc[0] - vc[3]) * fi[2] - (vc[1] - cImag * vc[2]) * fi[3]
    sr2 = -(vc[1] + cImag * vc[2]) * fi[2] + (vc[0] + vc[3]) * fi[3]

    fvi = jnp.where(gc[1] != cZero,
            jnp.array([
                (gc[0] * ((pf[0] - pf[3]) * sl1 - jnp.conj(fvi[5]) * sl2) + gc[1] * fmass * sr1) * d,
                (gc[0] * (-fvi[5] * sl1 + (pf[0] + pf[3]) * sl2) + gc[1] * fmass * sr2) * d,
                (gc[1] * ((pf[0] + pf[3]) * sr1 + jnp.conj(fvi[5]) * sr2) + gc[0] * fmass * sl1) * d,
                (gc[1] * (fvi[5] * sr1 + (pf[0] - pf[3]) * sr2) + gc[0] * fmass * sl2) * d,
                fvi[4],
                fvi[5]
            ]),
            jnp.array([
                gc[0]*((pf[0] - pf[3]) * sl1 - jnp.conj(fvi[5]) * sl2) * d,
                gc[0]*(-fvi[5] * sl1 + (pf[0] + pf[3]) * sl2) * d,
                gc[0]*fmass * sl1 * d,
                gc[0]*fmass * sl2 * d,
                fvi[4],
                fvi[5]
            ])
    )

    return IFermion(fvi)

fvixxx = vmap(fvixxx, in_axes=(0, 0, None, None, None))