"""
Original Fortran code:

      subroutine vxxxxx(p,vmass,nhel,nsv , vc)
c
c This subroutine computes a VECTOR wavefunction.
c
c input:
c       real    p(0:3)         : four-momentum of vector boson
c       real    vmass          : mass          of vector boson
c       integer nhel = -1, 0, 1: helicity      of vector boson
c                                (0 is forbidden if vmass=0.0)
c       integer nsv  = -1 or 1 : +1 for final, -1 for initial
c
c output:
c       complex vc(6)          : vector wavefunction       epsilon^mu(v)
c     
      implicit none
      double complex vc(6)
      double precision p(0:3),vmass,hel,hel0,pt,pt2,pp,pzpt,emp,sqh
      integer nhel,nsv,nsvahl

      double precision rZero, rHalf, rOne, rTwo
      parameter( rZero = 0.0d0, rHalf = 0.5d0 )
      parameter( rOne = 1.0d0, rTwo = 2.0d0 )


      sqh = dsqrt(rHalf)
      hel = dble(nhel)
      nsvahl = nsv*dabs(hel)
      pt2 = p(1)**2+p(2)**2
      pp = min(p(0),dsqrt(pt2+p(3)**2))
      pt = min(pp,dsqrt(pt2))

      vc(5) = dcmplx(p(0),p(3))*nsv
      vc(6) = dcmplx(p(1),p(2))*nsv


      if ( vmass.ne.rZero ) then

         hel0 = rOne-dabs(hel)

         if ( pp.eq.rZero ) then

            vc(1) = dcmplx( rZero )
            vc(2) = dcmplx(-hel*sqh )
            vc(3) = dcmplx( rZero , nsvahl*sqh )
            vc(4) = dcmplx( hel0 )

         else

            emp = p(0)/(vmass*pp)
            vc(1) = dcmplx( hel0*pp/vmass )
            vc(4) = dcmplx( hel0*p(3)*emp+hel*pt/pp*sqh )
            if ( pt.ne.rZero ) then
               pzpt = p(3)/(pp*pt)*sqh*hel
               vc(2) = dcmplx( hel0*p(1)*emp-p(1)*pzpt , 
     &                         -nsvahl*p(2)/pt*sqh       )
               vc(3) = dcmplx( hel0*p(2)*emp-p(2)*pzpt ,  
     &                          nsvahl*p(1)/pt*sqh       )
            else
               vc(2) = dcmplx( -hel*sqh )
               vc(3) = dcmplx( rZero , nsvahl*sign(sqh,p(3)) )
            endif

         endif

      else

         pp = p(0)
         pt = sqrt(p(1)**2+p(2)**2)
         vc(1) = dcmplx( rZero )
         vc(4) = dcmplx( hel*pt/pp*sqh )
         if ( pt.ne.rZero ) then
            pzpt = p(3)/(pp*pt)*sqh*hel
            vc(2) = dcmplx( -p(1)*pzpt , -nsv*p(2)/pt*sqh )
            vc(3) = dcmplx( -p(2)*pzpt ,  nsv*p(1)/pt*sqh )
         else
            vc(2) = dcmplx( -hel*sqh )
            vc(3) = dcmplx( rZero , nsv*sign(sqh,p(3)) )
         endif

      endif
c
    return
    end
"""



import jax.numpy as jnp
from jax import jit, vmap

from jax import lax
from pyhelas.typing import Momentum, Vector

def vxxxxx_scalar(p: Momentum, vmass, nhel, nsv) -> Vector:
    dtype = jnp.complex128 # TODO depend on input
    sqh = jnp.sqrt(0.5)
    hel = nhel
    nsvahl = nsv * jnp.abs(hel)
    
    pt2 = p[1]**2 + p[2]**2
    pp = jnp.minimum(p[0], jnp.sqrt(pt2 + p[3]**2))
    pt = jnp.minimum(pp, jnp.sqrt(pt2))
    
    vc = jnp.zeros(6, dtype=dtype)
    vc = vc.at[4].set(p[0]*nsv + p[3]*nsv*1j)
    vc = vc.at[5].set(p[1]*nsv + p[2]*nsv*1j)
    #vc = lax.dynamic_update_slice(vc, p[1] + p[2]*nsv, (6,))
    
    vmass_ne_rZero = vmass != 0.0
    
    if vmass_ne_rZero:
        hel0 = 1.0 - jnp.abs(hel)
        
        vc1 = hel0 * pp / vmass
        vc4_real = hel0 * p[3] * p[0] / (vmass * pp) + hel * pt / pp * sqh
        vc4_imag = 0.0
        
        cond_pt_ne_rZero = pt != 0.0
        vc2_real = jnp.where(cond_pt_ne_rZero, hel0 * p[1] * p[0] / (vmass * pp) - p[1] * p[3] / (pp * pt) * sqh * hel,
                              -hel * sqh)
        vc2_imag = jnp.where(cond_pt_ne_rZero, -nsvahl * p[2] / pt * sqh, 0.0)
        vc3_real = jnp.where(cond_pt_ne_rZero, hel0 * p[2] * p[0] / (vmass * pp) - p[2] * p[3] / (pp * pt) * sqh * hel,
                              0.0)
        vc3_imag = jnp.where(cond_pt_ne_rZero, nsvahl * p[1] / pt * sqh, nsvahl * sqh *jnp.sign(p[3]) )

        vc1 = jnp.where(pp == 0.0, 0.0, vc1)

        vc2_real = jnp.where(pp == 0.0, -hel * sqh, vc2_real)
        vc2_imag = jnp.where(pp == 0.0, 0.0, vc2_imag)
        vc3_real = jnp.where(pp == 0.0, 0.0, vc3_real)
        vc3_imag = jnp.where(pp == 0.0, nsvahl * sqh, vc3_imag)
        vc4_real = jnp.where(pp == 0.0, hel0, vc4_real)
        #vc4_imag = jnp.where(pp == 0.0, 0.0, vc4_imag)

    else:
        pp = p[0]
        pt = jnp.sqrt(p[1]**2 + p[2]**2)
        vc1 = 0.0
        vc4_real = hel * pt / pp * sqh
        vc4_imag = 0.0
        
        cond_pt_ne_rZero = pt != 0.0
        vc2_real = jnp.where(cond_pt_ne_rZero, -p[1] * p[3] / (pp * pt) * sqh * hel , -hel*sqh)
        vc2_imag = jnp.where(cond_pt_ne_rZero, -nsv * p[2] / pt * sqh, 0.0)
        vc3_real = jnp.where(cond_pt_ne_rZero, -p[2] * p[3] / (pp * pt) * sqh * hel, 0.0)
        vc3_imag = jnp.where(cond_pt_ne_rZero, nsv * p[1] / pt * sqh, nsv * sqh*jnp.sign(p[3]))
    
    vc = vc.at[0].set(vc1)
    vc = vc.at[1].set(vc2_real+1j* vc2_imag)
    vc = vc.at[2].set(vc3_real+1j* vc3_imag)
    vc = vc.at[3].set(vc4_real+1j* vc4_imag)
    
    return Vector(vc)

vxxxxx = vmap(vxxxxx_scalar, in_axes=(0, None, 0, None))