"""
Original Fortran code:

      subroutine oxxxxx(p,fmass,nhel,nsf , fo)
c
c This subroutine computes a fermion wavefunction with the flowing-OUT
c fermion number.
c
c input:
c       real    p(0:3)         : four-momentum of fermion
c       real    fmass          : mass          of fermion
c       integer nhel = -1 or 1 : helicity      of fermion
c       integer nsf  = -1 or 1 : +1 for particle, -1 for anti-particle
c
c output:
c       complex fo(6)          : fermion wavefunction               <fo|
c     
      implicit none
      double complex fo(6),chi(2)
      double precision p(0:3),sf(2),sfomeg(2),omega(2),fmass,
     &     pp,pp3,sqp0p3,sqm(0:1)
      integer nhel,nsf,nh,ip,im

      double precision rZero, rHalf, rTwo
      parameter( rZero = 0.0d0, rHalf = 0.5d0, rTwo = 2.0d0 )



      fo(5) = dcmplx(p(0),p(3))*nsf
      fo(6) = dcmplx(p(1),p(2))*nsf

      nh = nhel*nsf

      if ( fmass.ne.rZero ) then

         pp = min(p(0),dsqrt(p(1)**2+p(2)**2+p(3)**2))

         if ( pp.eq.rZero ) then
            
            sqm(0) = dsqrt(abs(fmass)) ! possibility of negative fermion masses
            sqm(1) = sign(sqm(0),fmass) ! possibility of negative fermion masses
            ip = -((1+nh)/2)
            im =  (1-nh)/2
            
            fo(1) = im     * sqm(im)
            fo(2) = ip*nsf * sqm(im)
            fo(3) = im*nsf * sqm(-ip)
            fo(4) = ip     * sqm(-ip)
            
         else
            
            pp = min(p(0),dsqrt(p(1)**2+p(2)**2+p(3)**2))
            sf(1) = dble(1+nsf+(1-nsf)*nh)*rHalf
            sf(2) = dble(1+nsf-(1-nsf)*nh)*rHalf
            omega(1) = dsqrt(p(0)+pp)
            omega(2) = fmass/omega(1)
            ip = (3+nh)/2
            im = (3-nh)/2
            sfomeg(1) = sf(1)*omega(ip)
            sfomeg(2) = sf(2)*omega(im)
            pp3 = max(pp+p(3),rZero)
            chi(1) = dcmplx( dsqrt(pp3*rHalf/pp) )
            if ( pp3.eq.rZero ) then
               chi(2) = dcmplx(-nh )
            else
               chi(2) = dcmplx( nh*p(1) , -p(2) )/dsqrt(rTwo*pp*pp3)
            endif
            
            fo(1) = sfomeg(2)*chi(im)
            fo(2) = sfomeg(2)*chi(ip)
            fo(3) = sfomeg(1)*chi(im)
            fo(4) = sfomeg(1)*chi(ip)

         endif
         
      else
         
         if(p(1).eq.0d0.and.p(2).eq.0d0.and.p(3).lt.0d0) then
            sqp0p3 = 0d0
         else
            sqp0p3 = dsqrt(max(p(0)+p(3),rZero))*nsf
         end if
         chi(1) = dcmplx( sqp0p3 )
         if ( sqp0p3.eq.rZero ) then
            chi(2) = dcmplx(-nhel )*dsqrt(rTwo*p(0))
         else
            chi(2) = dcmplx( nh*p(1), -p(2) )/sqp0p3
         endif
         if ( nh.eq.1 ) then
            fo(1) = chi(1)
            fo(2) = chi(2)
            fo(3) = dcmplx( rZero )
            fo(4) = dcmplx( rZero )
         else
            fo(1) = dcmplx( rZero )
            fo(2) = dcmplx( rZero )
            fo(3) = chi(2)
            fo(4) = chi(1)
         endif
         
      endif
c
      return
      end


"""



import jax.numpy as jnp
from jax import jit, vmap

from jax import lax

from pyhelas.typing import Momentum, OFermion

def oxxxxx_scalar(p : Momentum, fmass, nhel, nsf) -> OFermion:
    dtype = jnp.complex128 # TODO depend on input
    rZero = 0.0
    rHalf = 0.5
    rTwo = 2.0
    
    fo = jnp.zeros(6, dtype=dtype)
    chi = jnp.zeros(2, dtype=dtype)
    fov = jnp.zeros(4, dtype=dtype)
    
    fo = fo.at[4].set((p[0] +1j* p[3]) * nsf)
    fo = fo.at[5].set((p[1] +1j* p[2]) * nsf)
    
    nh = nhel * nsf

    fmass_ne_rZero = fmass != rZero

    if fmass_ne_rZero:
        pp = jnp.minimum(p[0], jnp.sqrt(p[1]**2 + p[2]**2 + p[3]**2))

        sqm = jnp.array([jnp.sqrt(jnp.abs(fmass)), jnp.sign(fmass)*jnp.sqrt(jnp.abs(fmass))])

        ip = jnp.where(pp == rZero, -(1 + nh) // 2, (3 + nh) // 2 -1)
        im = jnp.where(pp == rZero, (1 - nh) // 2, (3 - nh) // 2 -1)

        sf = jnp.array([1 + nsf + (1 - nsf) * nh, 1 + nsf - (1 - nsf) * nh]) 

        omega = jnp.array([jnp.sqrt(p[0] + pp), fmass / (jnp.sqrt(p[0] + pp))])
        sfomeg = jnp.array([sf[0] * omega[ip]* rHalf, sf[1] * omega[im]* rHalf])

        pp3 = jnp.maximum(pp + p[3], rZero)

        chi =  jnp.where(pp3 == rZero, 
                            jnp.array([jnp.sqrt(pp3 * rHalf / pp),-nh+0j]),
                            jnp.array([jnp.sqrt(pp3 * rHalf / pp),(nh * p[1]-1j*p[2]) / jnp.sqrt(rTwo * pp * pp3)])
                         )

        fov = jnp.where(pp == rZero, 
                    jnp.array([ im * sqm[im], ip * nsf * sqm[im], im * nsf * sqm[-ip], ip * sqm[-ip]]), 
                    jnp.array([sfomeg[1] * chi[im], sfomeg[1] * chi[ip], sfomeg[0] * chi[im], sfomeg[0] * chi[ip]])
                        )


    
    else:

        sqp0p3 = jnp.where((p[1] == 0.0) & (p[2] == 0.0) & (p[3] < 0.0), 0.0, jnp.sqrt(jnp.maximum(p[0] + p[3], rZero)) * nsf)
        chi = jnp.where(sqp0p3== 0.0,
                    jnp.array([(sqp0p3+0j),(-nhel +0j)*jnp.sqrt(rTwo * p[0])]),
                    jnp.array([(sqp0p3+0j),(nh*p[1]-1j*p[2])/sqp0p3]),
        )
        fov = jnp.where(nh == 1, 
                        jnp.array([chi[0], chi[1], 0j, 0j]), 
                        jnp.array([0j, 0j, chi[1], chi[0]])
        )


    fo = fo.at[0].set(  fov[0])
    fo = fo.at[1].set(  fov[1])
    fo = fo.at[2].set(  fov[2])
    fo = fo.at[3].set(  fov[3])

    return OFermion(fo)

oxxxxx = vmap(oxxxxx_scalar, in_axes=(0, None, 0, None))