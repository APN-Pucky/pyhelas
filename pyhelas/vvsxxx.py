"""
Original Fortran code:

      subroutine vvsxxx(v1,v2,sc,gc , vertex)
c
c This subroutine computes an amplitude of the vector-vector-scalar
c coupling.
c
c input:
c       complex v1(6)          : first  vector                        v1
c       complex v2(6)          : second vector                        v2
c       complex sc(3)          : input  scalar                        s
c       complex gc             : coupling constant                  gvvh
c
c output:
c       complex vertex         : amplitude                gamma(v1,v2,s)
c     
      implicit none
      double complex v1(6),v2(6),sc(3),gc,vertex

      vertex = gc*sc(1)
     &        *(v1(1)*v2(1)-v1(2)*v2(2)-v1(3)*v2(3)-v1(4)*v2(4))
c
      return
      end
"""

import jax.numpy as jnp
from jax import vmap



def vvsxxx_scalar(v1, v2, sc, gc):
    """
    This function computes an amplitude of the vector-vector-scalar coupling.

    Args:
        v1 (complex array): First vector v1.
        v2 (complex array): Second vector v2.
        sc (complex array): Input scalar sc.
        gc (complex): Coupling constant gc.

    Returns:
        complex: Amplitude gamma(v1, v2, s).
    """
    return gc * sc[0] * (v1[0]*v2[0] - v1[1]*v2[1] - v1[2]*v2[2] - v1[3]*v2[3])

vvsxxx = vmap(vvsxxx_scalar, in_axes=(0, 0, 0, None))