"""
Original Fortran code:

      subroutine vvvxxx(wm,wp,w3,g , vertex)
c
c This subroutine computes an amplitude of the three-point coupling of
c the gauge bosons.
c
c input:
c       complex wm(6)          : vector               flow-out W-
c       complex wp(6)          : vector               flow-out W+
c       complex w3(6)          : vector               j3 or A    or Z
c       real    g              : coupling constant    gw or gwwa or gwwz
c
c output:
c       complex vertex         : amplitude               gamma(wm,wp,w3)
c     
      implicit none
      double complex wm(6),wp(6),w3(6),vertex,
     &     xv1,xv2,xv3,v12,v23,v31,p12,p13,p21,p23,p31,p32
      double precision pwm(0:3),pwp(0:3),pw3(0:3),g

      double precision rZero, rTenth
      parameter( rZero = 0.0d0, rTenth = 0.1d0 )
c
      pwm(0) = dble( wm(5))
      pwm(1) = dble( wm(6))
      pwm(2) = dimag(wm(6))
      pwm(3) = dimag(wm(5))
      pwp(0) = dble( wp(5))
      pwp(1) = dble( wp(6))
      pwp(2) = dimag(wp(6))
      pwp(3) = dimag(wp(5))
      pw3(0) = dble( w3(5))
      pw3(1) = dble( w3(6))
      pw3(2) = dimag(w3(6))
      pw3(3) = dimag(w3(5))

      v12 = wm(1)*wp(1)-wm(2)*wp(2)-wm(3)*wp(3)-wm(4)*wp(4)
      v23 = wp(1)*w3(1)-wp(2)*w3(2)-wp(3)*w3(3)-wp(4)*w3(4)
      v31 = w3(1)*wm(1)-w3(2)*wm(2)-w3(3)*wm(3)-w3(4)*wm(4)
      xv1 = rZero
      xv2 = rZero
      xv3 = rZero

      if ( abs(wm(1)).ne.rZero ) then
         if ( abs(wm(1)).ge.max(abs(wm(2)),abs(wm(3)),abs(wm(4)))
     &        *rTenth )
     &      xv1 = pwm(0)/wm(1)
      endif
      if ( abs(wp(1)).ne.rZero) then
         if ( abs(wp(1)).ge.max(abs(wp(2)),abs(wp(3)),abs(wp(4)))
     &        *rTenth )
     &      xv2 = pwp(0)/wp(1)
      endif
      if ( abs(w3(1)).ne.rZero) then
         if ( abs(w3(1)).ge.max(abs(w3(2)),abs(w3(3)),abs(w3(4)))
     &        *rTenth )
     &      xv3 = pw3(0)/w3(1)
      endif

      p12 = (pwm(0)-xv1*wm(1))*wp(1)-(pwm(1)-xv1*wm(2))*wp(2)
     &     -(pwm(2)-xv1*wm(3))*wp(3)-(pwm(3)-xv1*wm(4))*wp(4)
      p13 = (pwm(0)-xv1*wm(1))*w3(1)-(pwm(1)-xv1*wm(2))*w3(2)
     &     -(pwm(2)-xv1*wm(3))*w3(3)-(pwm(3)-xv1*wm(4))*w3(4)
      p21 = (pwp(0)-xv2*wp(1))*wm(1)-(pwp(1)-xv2*wp(2))*wm(2)
     &     -(pwp(2)-xv2*wp(3))*wm(3)-(pwp(3)-xv2*wp(4))*wm(4)
      p23 = (pwp(0)-xv2*wp(1))*w3(1)-(pwp(1)-xv2*wp(2))*w3(2)
     &     -(pwp(2)-xv2*wp(3))*w3(3)-(pwp(3)-xv2*wp(4))*w3(4)
      p31 = (pw3(0)-xv3*w3(1))*wm(1)-(pw3(1)-xv3*w3(2))*wm(2)
     &     -(pw3(2)-xv3*w3(3))*wm(3)-(pw3(3)-xv3*w3(4))*wm(4)
      p32 = (pw3(0)-xv3*w3(1))*wp(1)-(pw3(1)-xv3*w3(2))*wp(2)
     &     -(pw3(2)-xv3*w3(3))*wp(3)-(pw3(3)-xv3*w3(4))*wp(4)

      vertex = -(v12*(p13-p23)+v23*(p21-p31)+v31*(p32-p12))*g
c
      return
      end
"""


import jax.numpy as jnp
from jax import jit, vmap

from pyhelas.typing import Vector

def if_not_zero_ratio(w,pw):
    rZero = 0.0 # Assuming rZero is defined
    rTenth = 0.1  # Assuming rTenth is defined
    xv = rZero

    # Compute absolute values and maximum value
    abs_w = jnp.abs(w[0])
    max_abs_w_except_first = jnp.max(jnp.abs(w[1:4]))

    # Create a mask for elements not equal to rZero
    non_zero_mask = abs_w != rZero

    # Create a mask for the condition abs(w[0]) >= max(abs(w[1:4])) * rTenth
    condition_mask = abs_w >= (max_abs_w_except_first * rTenth)

    # Combine masks
    combined_mask = non_zero_mask & condition_mask

    # Apply mask to calculate xv1
    xv = jnp.where(combined_mask, pw[0] / w[0], xv)
    return xv

def vvvxxx_scalar(wm : Vector, wp: Vector, w3: Vector, g) -> complex:

    pwm = jnp.array([jnp.real(wm[4]), jnp.real(wm[5]), jnp.imag(wm[5]), jnp.imag(wm[4])])
    pwp = jnp.array([jnp.real(wp[4]), jnp.real(wp[5]), jnp.imag(wp[5]), jnp.imag(wp[4])])
    pw3 = jnp.array([jnp.real(w3[4]), jnp.real(w3[5]), jnp.imag(w3[5]), jnp.imag(w3[4])])

    v12 = wm[0]*wp[0] - wm[1]*wp[1] - wm[2]*wp[2] - wm[3]*wp[3]
    v23 = wp[0]*w3[0] - wp[1]*w3[1] - wp[2]*w3[2] - wp[3]*w3[3]
    v31 = w3[0]*wm[0] - w3[1]*wm[1] - w3[2]*wm[2] - w3[3]*wm[3]


    xv1 = if_not_zero_ratio(wm, pwm)
    xv2 = if_not_zero_ratio(wp, pwp)
    xv3 = if_not_zero_ratio(w3, pw3)


    p12 = (pwm[0] - xv1*wm[0]) * wp[0] - (pwm[1] - xv1*wm[1]) * wp[1] - (pwm[2] - xv1*wm[2]) * wp[2] - (pwm[3] - xv1*wm[3]) * wp[3]
    p13 = (pwm[0] - xv1*wm[0]) * w3[0] - (pwm[1] - xv1*wm[1]) * w3[1] - (pwm[2] - xv1*wm[2]) * w3[2] - (pwm[3] - xv1*wm[3]) * w3[3]
    p21 = (pwp[0] - xv2*wp[0]) * wm[0] - (pwp[1] - xv2*wp[1]) * wm[1] - (pwp[2] - xv2*wp[2]) * wm[2] - (pwp[3] - xv2*wp[3]) * wm[3]
    p23 = (pwp[0] - xv2*wp[0]) * w3[0] - (pwp[1] - xv2*wp[1]) * w3[1] - (pwp[2] - xv2*wp[2]) * w3[2] - (pwp[3] - xv2*wp[3]) * w3[3]
    p31 = (pw3[0] - xv3*w3[0]) * wm[0] - (pw3[1] - xv3*w3[1]) * wm[1] - (pw3[2] - xv3*w3[2]) * wm[2] - (pw3[3] - xv3*w3[3]) * wm[3]
    p32 = (pw3[0] - xv3*w3[0]) * wp[0] - (pw3[1] - xv3*w3[1]) * wp[1] - (pw3[2] - xv3*w3[2]) * wp[2] - (pw3[3] - xv3*w3[3]) * wp[3]

    vertex = -(v12 * (p13 - p23) + v23 * (p21 - p31) + v31 * (p32 - p12)) * g

    return vertex

vvvxxx = vmap(vvvxxx_scalar, in_axes=(0, 0, 0, None))