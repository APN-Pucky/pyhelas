"""
Original Fortran code:

      subroutine iovxxx(fi,fo,vc,gc , vertex)
c
c This subroutine computes an amplitude of the fermion-fermion-vector
c coupling.
c
c input:
c       complex fi(6)          : flow-in  fermion                   |fi>
c       complex fo(6)          : flow-out fermion                   <fo|
c       complex vc(6)          : input    vector                      v
c       complex gc(2)          : coupling constants                  gvf
c
c output:
c       complex vertex         : amplitude                     <fo|v|fi>
c     
      implicit none
      double complex fi(6),fo(6),gc(2),vc(6),vertex

      double precision rZero, rOne
      parameter( rZero = 0.0d0 )
      double complex cImag, cZero
      parameter( cImag = ( 0.0d0, 1.0d0 ), cZero = ( 0.0d0, 0.0d0 ) )

        vertex =  gc(1)*( (fo(3)*fi(1)+fo(4)*fi(2))*vc(1)
     &                 +(fo(3)*fi(2)+fo(4)*fi(1))*vc(2)
     &                 -(fo(3)*fi(2)-fo(4)*fi(1))*vc(3)*cImag
     &                 +(fo(3)*fi(1)-fo(4)*fi(2))*vc(4)        )

      if ( gc(2).ne.cZero ) then
         vertex = vertex
     &          + gc(2)*( (fo(1)*fi(3)+fo(2)*fi(4))*vc(1)
     &                   -(fo(1)*fi(4)+fo(2)*fi(3))*vc(2)
     &                   +(fo(1)*fi(4)-fo(2)*fi(3))*vc(3)*cImag
     &                   -(fo(1)*fi(3)-fo(2)*fi(4))*vc(4)        )
      end if
c
      return
      end
"""


import jax.numpy as jnp
from jax import vmap

from pyhelas.typing import IFermion, OFermion

def iovxxx_scalar(fi : IFermion, fo :OFermion, vc, gc) -> complex:
    cImag = 1j
    return (gc[0] * ((fo[2]*fi[0] + fo[3]*fi[1]) * vc[0]
                      + (fo[2]*fi[1] + fo[3]*fi[0]) * vc[1]
                      - (fo[2]*fi[1] - fo[3]*fi[0]) * vc[2] * cImag
                      + (fo[2]*fi[0] - fo[3]*fi[1]) * vc[3]) + 
        jnp.where(gc[1] != 0j, 
                  (gc[1] * ((fo[0]*fi[2] + fo[1]*fi[3]) * vc[0]
                            - (fo[0]*fi[3] + fo[1]*fi[2]) * vc[1]
                            + (fo[0]*fi[3] - fo[1]*fi[2]) * vc[2] * cImag
                            - (fo[0]*fi[2] - fo[1]*fi[3]) * vc[3])), 0j)
    )

iovxxx = vmap(iovxxx_scalar, in_axes=(0, 0, 0, None))