"""
Original Fortran code:

      subroutine hioxxx(fi,fo,gc,smass,swidth , hio)
c
c This subroutine computes an off-shell scalar current from an external
c fermion pair.
c
c input:
c       complex fi(6)          : flow-in  fermion                   |fi>
c       complex fo(6)          : flow-out fermion                   <fo|
c       complex gc(2)          : coupling constants                 gchf
c       real    smass          : mass  of OUTPUT scalar s
c       real    swidth         : width of OUTPUT scalar s
c
c output:
c       complex hio(3)         : scalar current             j(<fi|s|fo>)
c
      implicit none
      double complex fi(6),fo(6),hio(3),gc(2),dn
      double precision q(0:3),smass,swidth,q2

      hio(2) = fo(5)-fi(5)
      hio(3) = fo(6)-fi(6)

      q(0) = dble( hio(2))
      q(1) = dble( hio(3))
      q(2) = dimag(hio(3))
      q(3) = dimag(hio(2))
      q2 = q(0)**2-(q(1)**2+q(2)**2+q(3)**2)

      dn = -dcmplx( q2-smass**2, smass*swidth )

      hio(1) = ( gc(1)*(fo(1)*fi(1)+fo(2)*fi(2))
     &          +gc(2)*(fo(3)*fi(3)+fo(4)*fi(4)) )/dn
c
      return
      end
"""

import jax.numpy as jnp
from jax import vmap

def hioxxx_scalar(fi, fo, gc, smass, swidth):
    hio = jnp.array([fo[3] - fi[3], fo[4] - fi[4], fo[5] - fi[5]])

    q = jnp.array([hio[1].real, hio[2].real, hio[2].imag, hio[1].imag])
    q2 = q[0]**2 - (q[1]**2 + q[2]**2 + q[3]**2)

    dn = -(q2 - smass**2+1j* smass * swidth)

    hio = hio.at[0].set((gc[0] * (fo[0] * fi[0] + fo[1] * fi[1]) 
                 + gc[1] * (fo[2] * fi[2] + fo[3] * fi[3])) / dn)

    return hio

hioxxx = vmap(hioxxx_scalar, in_axes=(0, 0, None, None, None))