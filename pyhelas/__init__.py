from .ioscxx import ioscxx
from .j3xxxx import j3xxxx
from .vvvxxx import vvvxxx
from .vxxxxx import vxxxxx
from .oxxxxx import oxxxxx
from .ixxxxx import ixxxxx
from .fvixxx import fvixxx
from .iovxxx import iovxxx
from .hioxxx import hioxxx
from .vvsxxx import vvsxxx

from itertools import product
import jax.numpy as jnp



def construct_helicities(number_of_particles):
    helicities = list(product([1, -1], repeat=number_of_particles))
    return [row for row in jnp.array(helicities).T]

def all_helicities(ps):
    number_of_particles = len(ps)

    # for every particle, we have 2 helicities [1, -1]
    # now construct all possible helicities for all particles

    # one particle -> [[1,-1]]
    # two particles -> [[1,1,-1,-1],[1,-1,1,-1]]

    all_helicities = construct_helicities(number_of_particles)

    for i in range(number_of_particles):
        ps[i] = jnp.repeat(jnp.array([ps[i]]), 2**number_of_particles, axis=0)

    return ps,all_helicities

def append_mp(tps,thels,ps,hels):
    for i,p in enumerate(ps):
        tps[i] = jnp.concatenate((tps[i],p),axis=0)
    for i,h in enumerate(hels):
        thels[i] = jnp.concatenate((thels[i],h),axis=0)