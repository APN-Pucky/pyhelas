
# py3.12 -> better sicne i_fermion != o_fermion
#type momentum = tuple[float,float,float,float]
#type i_fermion = tuple[complex,complex,complex,complex,complex,complex]
#type o_fermion = tuple[complex,complex,complex,complex,complex,complex]
#type v_vector = tuple[complex,complex,complex,complex,complex,complex]


from typing import NewType
from typing import Tuple
from typing import List 
from jax import Array


Momentum  = NewType('Momentum', Array)
IFermion = NewType('IFermion', Array)
OFermion = NewType('OFermion', Array)
Vector  = NewType('Vector', Array) 
Helicity = NewType('Helicity', Array)