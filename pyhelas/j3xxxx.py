"""
Original Fortran code:

      subroutine j3xxxx(fi,fo,gaf,gzf,zmass,zwidth , j3)
c
c This subroutine computes the sum of photon and Z currents with the
c suitable weights ( j(W3) = cos(theta_W) j(Z) + sin(theta_W) j(A) ).
c The output j3 is useful as an input of vvvxxx, jvvxxx or w3w3xx.
c The photon propagator is given in Feynman gauge, and the Z propagator
c is given in unitary gauge.
c
c input:
c       complex fi(6)          : flow-in  fermion                   |fi>
c       complex fo(6)          : flow-out fermion                   <fo|
c       complex gaf(2)         : fi couplings with A                 gaf
c       complex gzf(2)         : fi couplings with Z                 gzf
c       real    zmass          : mass  of Z
c       real    zwidth         : width of Z
c
c output:
c       complex j3(6)          : W3 current             j^mu(<fo|w3|fi>)
c     
      implicit none
      double complex fi(6),fo(6),j3(6),gaf(2),gzf(2)
      double complex c0l,c1l,c2l,c3l,csl,c0r,c1r,c2r,c3r,csr,dz,ddif
      double complex gn,gz3l,ga3l
      double complex cm2  ! mass**2- I Gamma mass (Fabio)
      double precision q(0:3),zmass,zwidth,zm2,zmw
      double precision q2,da,ww,cw,sw

      double precision rZero, rOne
      parameter( rZero = 0.0d0, rOne = 1.0d0 )
      double complex cImag, cZero
      parameter( cImag = ( 0.0d0, 1.0d0 ), cZero = ( 0.0d0, 0.0d0 ) )

      j3(5) = fo(5)-fi(5)
      j3(6) = fo(6)-fi(6)

      q(0) = -dble( j3(5))
      q(1) = -dble( j3(6))
      q(2) = -dimag(j3(6))
      q(3) = -dimag(j3(5))
      q2 = q(0)**2-(q(1)**2+q(2)**2+q(3)**2)
      zm2 = zmass**2
      zmw = zmass*zwidth

      da = rOne/q2
C      ww = max(dsign(zmw,q2), rZero)
      dz = rOne/dcmplx( q2-zm2, zmw )
      ddif = dcmplx( -zm2, zmw )*da*dz

c ddif is the difference : ddif=da-dz
c  For the running width, use below instead of the above ww,dz and ddif.
c      ww = max( zwidth*q2/zmass, rZero )
c      dz = rOne/dcmplx( q2-zm2, zmw )
c      ddif = dcmplx( -zm2, zmw )*da*dz



      cw = rOne/sqrt(rOne+(gzf(2)/gaf(2))**2)
      sw = sqrt((rOne-cw)*(rOne+cw))
      gn = gaf(2)*sw
      gz3l = gzf(1)*cw
      ga3l = gaf(1)*sw
      c0l =   fo(3)*fi(1)+fo(4)*fi(2)
      c0r =   fo(1)*fi(3)+fo(2)*fi(4)
      c1l = -(fo(3)*fi(2)+fo(4)*fi(1))
      c1r =   fo(1)*fi(4)+fo(2)*fi(3)
      c2l =  (fo(3)*fi(2)-fo(4)*fi(1))*cImag
      c2r = (-fo(1)*fi(4)+fo(2)*fi(3))*cImag
      c3l =  -fo(3)*fi(1)+fo(4)*fi(2)
      c3r =   fo(1)*fi(3)-fo(2)*fi(4)

c     Fabio's implementation of the fixed width
      cm2=dcmplx( zm2, -zmw )
c     csl = (q(0)*c0l-q(1)*c1l-q(2)*c2l-q(3)*c3l)/zm2
c     csr = (q(0)*c0r-q(1)*c1r-q(2)*c2r-q(3)*c3r)/zm2
      csl = (q(0)*c0l-q(1)*c1l-q(2)*c2l-q(3)*c3l)/cm2
      csr = (q(0)*c0r-q(1)*c1r-q(2)*c2r-q(3)*c3r)/cm2
      
      j3(1) =  gz3l*dz*(c0l-csl*q(0))+ga3l*c0l*da
     &       + gn*(c0r*ddif+csr*q(0)*dz)
      j3(2) =  gz3l*dz*(c1l-csl*q(1))+ga3l*c1l*da
     &       + gn*(c1r*ddif+csr*q(1)*dz)
      j3(3) =  gz3l*dz*(c2l-csl*q(2))+ga3l*c2l*da
     &       + gn*(c2r*ddif+csr*q(2)*dz)
      j3(4) =  gz3l*dz*(c3l-csl*q(3))+ga3l*c3l*da
     &       + gn*(c3r*ddif+csr*q(3)*dz)
c
      return
      end
"""

import jax.numpy as jnp
from jax import jit, vmap

from pyhelas.typing import IFermion, OFermion, Vector

def j3xxxx_scalar(fi: IFermion, fo : OFermion, gaf, gzf, zmass, zwidth) -> Vector:

    j3 = jnp.zeros(6, dtype=fi.dtype)
    j3 = j3.at[4].set(fo[4] - fi[4])
    j3 = j3.at[5].set(fo[5] - fi[5])

    q = jnp.zeros(4)
    q = q.at[0].set(-jnp.real(j3[4]))
    q = q.at[1].set(-jnp.real(j3[5]))
    q = q.at[2].set(-jnp.imag(j3[5]))
    q = q.at[3].set(-jnp.imag(j3[4]))

    q2 = q[0]**2 - (q[1]**2 + q[2]**2 + q[3]**2)
    zm2 = zmass**2
    zmw = zmass * zwidth

    da = 1.0 / q2
    dz = 1.0 / (q2 - zm2 + 1j * zmw)
    ddif = (-zm2 + 1j * zmw) * da * dz

    cw = 1.0 / jnp.sqrt(1.0 + (gzf[1] / gaf[1])**2)
    sw = jnp.sqrt((1.0 - cw) * (1.0 + cw))
    gn = gaf[1] * sw
    gz3l = gzf[0] * cw
    ga3l = gaf[0] * sw

    c0l = fo[2] * fi[0] + fo[3] * fi[1]
    c0r = fo[0] * fi[2] + fo[1] * fi[3]
    c1l = -(fo[2] * fi[1] + fo[3] * fi[0])
    c1r = fo[0] * fi[3] + fo[1] * fi[2]
    c2l = (fo[2] * fi[1] - fo[3] * fi[0]) * 1j
    c2r = (-fo[0] * fi[3] + fo[1] * fi[2]) * 1j
    c3l = -fo[2] * fi[0] + fo[3] * fi[1]
    c3r = fo[0] * fi[2] - fo[1] * fi[3]

    cm2 = zm2 - 1j * zmw
    csl = (q[0] * c0l - q[1] * c1l - q[2] * c2l - q[3] * c3l) / cm2
    csr = (q[0] * c0r - q[1] * c1r - q[2] * c2r - q[3] * c3r) / cm2

    j3 = j3.at[0].set(gz3l * dz * (c0l - csl * q[0]) + ga3l * c0l * da + gn * (c0r * ddif + csr * q[0] * dz))
    j3 = j3.at[1].set(gz3l * dz * (c1l - csl * q[1]) + ga3l * c1l * da + gn * (c1r * ddif + csr * q[1] * dz))
    j3 = j3.at[2].set(gz3l * dz * (c2l - csl * q[2]) + ga3l * c2l * da + gn * (c2r * ddif + csr * q[2] * dz))
    j3 = j3.at[3].set(gz3l * dz * (c3l - csl * q[3]) + ga3l * c3l * da + gn * (c3r * ddif + csr * q[3] * dz))

    return Vector(j3)

j3xxxx = vmap(j3xxxx_scalar, in_axes=(0, 0, None, None, None, None))